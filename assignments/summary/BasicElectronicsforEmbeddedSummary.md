# Basic Electronics concepts of basic electronics
 
  
![](photos/1.jpg)
##	Sensor
•	Sensors are “Input” devices which convert one type of energy or quantity into an electrical analogue signal.
•	For Example : A microphone 

  
![](photos/2.jpg)

##  Actuator
•	“Output” devices are commonly called Actuators and the simplest of all actuators is the lamp.
•	For example: an electric motor, a hydraulic system, and a pneumatic system are all different types of actuators.

  
![](photos/3.jpg)

 ##  Analog vs digital :



  
![](photos/4.jpg)

  
![](photos/12.jpeg)





##  Micro-Controllers vs Micro-Processors  

  
![](photos/5.jpg)


##  	Introduction to RPI
 
   
![](photos/6.jpg)

**Raspberry Pi** is popularly used for real time Image/Video Processing, IoT based applications and Robotics applications.
 Serial and parallel communication
**Raspberry Pi Interfaces :**
1.	GPIO
2.	UART
3.	SPI
4.	I2C
5.	PWM
	GPIO:
**A GPIO **(general-purpose input/output) port handles both incoming and outgoing digital signals.
GPIO can be used as Input or Output.
As an input port, it can be used to compare digital readings received from sensors.
As an output port, it can be used as a switch to turn ON/OFF devices.
 
![](photos/7.jpg)

**UART:** Universal Asynchronous Receiver Transmitter
UART (Universal Asynchronous Receiver Transmitter) is an Asynchronous Serial communication protocol. 

 ![](photos/8.jpg)

## Serial and parallel communication:
 
![](photos/9.jpg)
 
## ADC and DAC:
           
 **ADC** stands for Analog to Digital Converter, which converts the analog signal into the digital signal. DAC stands for Digital to Analog Converter and it converts the Digital signal into an analog signal. 

![](photos/10.jpg)

**PWM:**
Pulse Width Modulation, or PWM, is a technique for getting analog results with digital means. Digital control is used to create a square wave, a signal switched between on and off. ... The duration of "on time" is called the pulse width. To get varying analog values, you change, or modulate, that pulse width.
 

  
![](photos/11.jpg)

