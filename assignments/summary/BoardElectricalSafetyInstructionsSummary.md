# Board Electrical Safety instructions

## What is electrical safety?

**Electrical safety** is a system of organizational measures and technical means to prevent harmful and dangerous effects on workers from electric current, electric arc, electromagnetic field and static electricity.

## IoT and Safety Hazards

There is a high-level summary of standards on the horizon for the connected world. Using IoT sensors is a cheap and effective way to ensure that all electric circuits are functioning correctly.
The Internet of things (IoT) is taking the concept of safety to a whole new level. In a world where everything can be connected, and physical contact becomes optional, verbal instructions and even facial recognition are used to operate the end device, safety considerations must be made.

**Type 1** hazards are directly associated with the traditional use of the device and include things such as: overheating, shock, sonic hazards, etc.

**Type 2** hazards are indirectly related to the device and its operation but could enable a security or safety issue by implementing IoT.


## Power Supply

**Do’s:**
•	The Output voltage of the power supply should match the input voltage of the board.
•	Before turning on the power supply make sure circuit is connected properly.

**Don’ts:**
•	Do not connect power supply without matching the power rating.
•	Do not connect higher output(12V/3Amp) to a lower (5V/2Amp) input .

![](photos/21.jpg)

## Handling

**Do’s:**

•	Keep the board on a flat stable insulated surface.
•	Keep all electrical circuit contact points enclosed.

**Don’ts:**

•	Don’t handle the board when its powered ON.
•	Never touch electrical equipment when any part of your body is wet.
•	Do not touch any sort of metal to the development board.




## Guidelines for using UART interface

A universal asynchronous receiver-transmitter is a computer hardware device for asynchronous serial communication in which the data format and transmission speeds are configurable. Connect Rx pin of device1 to Tx pin of device2 ,similarly Tx pin of device1 to Rx pin of device2.
•	If the device1 works on 5v and device2 works at 3.3v,use the level shifting mechanism using Volatge divider 
•	Senor interfacing using UART might require a protection circuit.
 
![](photos/22.jpg)

![](photos/23.jpg)


## Guidelines for using I2C interface
•	When using I2c interfaces with sensors SDA and SDL lines must be protected.
•	Protection of these lines is done by using pullup registers on both lines.
•	while using inbuilt pullup registers in the board you wont need an external circuit.
•	if using bread-board to connect sensor , use the pullup resistor .
•	Generally , 2.2kohm <= 4K ohm resistors are used.
 
 ![](photos/24.jpg)

## Guidelines for using SPI interface
•	Spi in development boards is in Push-pull mode.
•	Push-pull mode does not require any protection circuit..
•	Resistors value should be in between 1kOhm ~10kOhm,Preferably 4.7k Ohm resistor.
 

![](photos/25.jpg)
