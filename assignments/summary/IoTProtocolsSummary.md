#    IoT protocols

## 1. The Fundamentals of 4-20 mA Current Loops:

![](photos/31.jpg)






### Introduction:
The 4-20 mA current loop has been the standard for signal transmission and electronic control in control systems since the 1950's. In a current loop, the current signal is drawn from a dc power supply, flows through the transmitter, into the controller and then back to the power supply in a series circuit.
Components of a 4-20 mA Current Loop:

   
![](photos/32.jpg)
 

**1. Sensor**
A sensor typically measures temperature, humidity, flow, level or pressure. 

**2. Transmitter**
Whatever the sensor is monitoring, there needs to be a way to convert its measurement into a current signal, between four and twenty milliamps. 

**3. Power Source**
In order for a signal to be produced, there needs to be a source of power, just as in the water system analogy there needed to be a source of water pressure. 

**4. Loop**
In addition to an adequate VDC supply, there also needs to be a loop, which refers to the actual wire connecting the sensor to the device receiving the 4-20 mA signal and then back to the transmitter. 

**5. Receiver**
Finally, at someplace in the loop there will be a device which can receive and interpret the current signal.

### Pros & Cons of 4-20 mA Loops

**Pros**
•	The 4-20 mA current loop is the dominant standard in many industries.
•	It is less sensitive to background electrical noise.

**Cons**
•	Current loops can only transmit one particular process signal.
•	These isolation requirements become exponentially more complicated as the number of loops increases.

## Modbus Communication Protocol


![](photos/33.jpg)




•	Modbus is a communication protocol for use with programmable logic controllers (PLC). 
•	The protocol is commonly used in IoT as a local interface to manage devices.
•	Modbus protocol can be used over 2 interfaces

**1.	RS485**- called as Modbus RTU

**2.	Ethernet** - called as Modbus TCP/IP

### What is RS485?

RS485 is a serial (like UART) transmission standard, you can put several RS485 devices on the same bus.

## OPCUA protocol
OPC Unified Architecture (OPC UA) is a machine to machine communication protocol for industrial automation developed by the OPC Foundation. Distinguishing characteristics are: Focus on communicating with industrial equipment and systems for data collection and control.
 

![](photos/34.jpg)


##               Cloud Protocols

### 1. MQTT (Message Queuing Telemetry Transport) :
•	A simple messaging protocol designed for constrained devices with low bandwidth. 
•	A lightweight publish and subscribe system to publish and receive messages as a client. 

A general overview of how communication takes place within MQTT devices.


![](photos/35.jpg)



### 2. HTTP ( Hyper Text Transport Protocol)
•	It is a request response protocol
•	The client sends an HTTP request
•	The server sends back a HTTP response



![](photos/36.jpg)







### The Request
request has 3 parts

•	Request line

•	HTTP headers

•	message body

